1.- clonamos el archivo env para administrar  node en usuario normal
 $wget -c https://nodejs.org/dist/v6.2.1/node-v6.2.1-linux-x64.tar.xz
 $echo 'export PATH="$HOME/.nodenv/bin:$PATH"' >> ~/.bashrc
 $echo 'eval "$(nodenv init -)"' >> ~/.bashrc
 
2.- Regularmente  esta descarga  no contiene  la carpeta /versions asi que  
la creamos

$mkdir -p $HOME/.nodenv/versions
Nos situamos en la carpeta versions
$cd $HOME/.nodenv/versions

con la herramienta wget  descargamos la version  de  node que queramos 
$wget -c https://nodejs.org/dist/v6.2.1/node-v6.2.1-linux-x64.tar.xz

*Actualmente  la version es 8.*.*

Descomprimimos el archivo  con el comando tar 
$tar xf node-v6.2.1-linux-x64.tar.xz


Puedes actualizar el node  y npm con
 $npm update -g
